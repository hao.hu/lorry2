use clap::Parser;
use logging::progress;
use std::{
    fs::{self},
    path::PathBuf,
    pin::Pin,
};
use tokio::{io::AsyncWriteExt, task::JoinHandle};
use workerlib::combine_configs::PartialArguments;

//TODO have reciever spew to specified error

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Arguments {
    #[command(flatten)]
    settings: PartialArguments,

    ///extra config file that can contain parts of config that were not specified on the CLI
    #[arg(long = "config")]
    config_file: Option<PathBuf>,

    #[arg()]
    lorry_specification_files: Vec<PathBuf>,

    #[arg()]
    mirror_server_base_url: url::Url,
}
#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = Arguments::parse();

    //println!("{:?}", args);
    let (
        mirroring_args,
        lorry_specification_files,
        mirror_server_base_url,
        test_parsing_only,
        verbose,
        tx,
        handle,
    ) = process_arguments(args);
    //println!("Mirroring args:{:?}", mirroring_args);
    //println!("topo args:{:?}", test_parsing_only);
    let mut failed_to_read = vec![];

    let mut failure: bool = false;
    let lorry_results = lorry_specification_files
        .iter()
        .map(|f| (fs::read_to_string(f), f))
        .filter_map(|(r, p)| match r {
            Ok(s) => Some((s, p)),
            Err(e) => {
                failed_to_read.push((p, e));
                None
            }
        })
        .collect::<Vec<_>>();

    if !failed_to_read.is_empty() {
        failure = true;
        progress(
            format!("{} lorry files could not be read:", failed_to_read.len()),
            verbose,
            &tx,
        )
        .await;
        for (filename, error) in failed_to_read {
            progress(format!("{:?}: {:?}", filename, error), verbose, &tx).await;
        }
    }

    let mut failed_to_parse = vec![];
    let lorry_results = lorry_results
        .iter()
        .filter_map(|(s, path)| match workerlib::extract_lorry_specs(s) {
            Ok(extracted_lorries) => Some(extracted_lorries),
            Err(e) => {
                failed_to_parse.push((path, e));
                None
            }
        })
        .collect::<Vec<_>>();

    if !failed_to_parse.is_empty() {
        failure = true;
        progress(
            format!("{} lorry files could not be parsed:", failed_to_parse.len()),
            verbose,
            &tx,
        )
        .await;
        for (filename, error) in failed_to_parse {
            progress(format!("{:?}: {:?}", filename, error), verbose, &tx).await;
        }
    }

    if test_parsing_only {
        drop(mirroring_args);
        drop(tx);

        std::process::exit(if handle.await.is_ok() { 0 } else { 1 });
    }

    let mirroring_results =
        workerlib::mirror_many(lorry_results, &mirror_server_base_url, &mirroring_args).await;

    let mirrors_failed = mirroring_results
        .into_iter()
        .filter_map(|r| r.1.err().map(|e| (r.0, e)))
        .collect::<Vec<_>>();

    if !mirrors_failed.is_empty() {
        failure = true;
        progress(
            format!("{} mirrors failed:", mirrors_failed.len()),
            verbose,
            &tx,
        )
        .await;
        for (filename, error) in mirrors_failed {
            progress(format!("{:?}: {:?}", filename, error), verbose, &tx).await;
        }
    };

    //need to drop these so that the communication channels are closed, allowing the reciever buffer to be drained.
    //If we don't drop them, then the channel is never closed and the program hangs at the await
    drop(mirroring_args);
    drop(tx);
    handle.await.unwrap();

    if failure {
        std::process::exit(1);
    } else {
        std::process::exit(0);
    }
}
/// read the config files in order, and fetch settings from them; the most recently set value for a setting is the final value, with teh values specified at the CLI taking highest priority.
fn process_arguments(
    args: Arguments,
) -> (
    workerlib::Arguments,
    Vec<PathBuf>,
    url::Url,
    bool,
    bool,
    logging::Sender,
    JoinHandle<()>,
) {
    let config_fragments = args
        .config_file
        .iter()
        .filter_map(|p| match fs::read_to_string(p) {
            Ok(text) => Some((p, text)),
            Err(e) => {
                tracing::warn!("Could not read configuration from {}: {}", p.display(), e);
                None
            }
        })
        //TODO we've considered also loading config as YAML files
        .filter_map(|(p, s)| match toml::from_str::<PartialArguments>(&s) {
            Ok(conf) => Some(conf),
            Err(e) => {
                tracing::warn!("Could not parse configuration from {}: {}", p.display(), e);
                None
            }
        })
        .chain(std::iter::once(args.settings));

    let (final_args, parsing_only, verbose, log_target, log_level, tx, mut rx) =
        workerlib::combine_configs::combine_configs(config_fragments);

    let log_level = if log_target.is_none() {
        logging::LogLevel::Fatal
    } else {
        log_level.unwrap_or(logging::LogLevel::Fatal)
    };

    let mut w: Pin<Box<dyn tokio::io::AsyncWrite + Send>> = match log_target {
        None => Box::pin(tokio::io::sink()),
        Some(s) => match s.as_str() {
            "stderr" => Box::pin(tokio::io::stderr()),
            "syslog" => todo!("Syslog is not a valid log type yet"),
            "none" => Box::pin(tokio::io::sink()),
            _ => todo!("files are not valid log types yet"),
        },
    };

    let logger_handle = tokio::task::spawn(async move {
        while let Some((new_msg_level, new_msg_content)) = rx.recv().await {
            if new_msg_level >= log_level {
                w.write_all(new_msg_content.as_bytes()).await.unwrap();
                //TODO catch the error instead on unwrapping it?
            }
        }
    });

    (
        final_args,
        args.lorry_specification_files,
        args.mirror_server_base_url,
        parsing_only,
        verbose,
        tx,
        logger_handle,
    )
}
