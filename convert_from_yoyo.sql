CREATE TABLE _sqlx_migrations (
    version BIGINT PRIMARY KEY,
    description TEXT NOT NULL,
    installed_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    success BOOLEAN NOT NULL,
    checksum BLOB NOT NULL,
    execution_time BIGINT NOT NULL
);

INSERT INTO _sqlx_migrations (version, description,                 installed_on,           success,checksum,                                                                                       execution_time) 
                    VALUES (20230328145738, "initial table creation",CURRENT_TIMESTAMP,   true, X'2F8DB2307D094D06520E704193E44B5524AFC16F25B0DB133640A99BA4D79A7D5BAADE9C6D59AD556EA7726B339C9ADA',       9163372);
--TODO have to replace the checksum since it has changed with the new comment...

-- Drop yoyo-specific tables, since it is no longer managing this DB
DROP TABLE yoyo_lock;
DROP TABLE _yoyo_log;
DROP TABLE _yoyo_version;
DROP TABLE _yoyo_migration;