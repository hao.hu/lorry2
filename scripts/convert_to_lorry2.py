#!/usr/bin/env python3

from pathlib import Path
import argparse


def convert_file(file_path):
    """
    convert a single lorry file to lorry2 format
    """
    content = ""

    with open(file_path, "r", encoding="utf-8") as r:
        new_repo_flag = False
        repo_content = ""
        merge_url_flag = False
        tarball_flag = False

        for line in r.readlines():
            stripped_line = line.strip()
            # new repo
            if not line.startswith(" ") and stripped_line.endswith(":"):
                if repo_content:
                    content += repo_content
                repo_content = ""
                new_repo_flag = True

            if new_repo_flag:
                # merge line only contains "url:" with next line
                # ie: convert
                #
                # url:
                #    https://lorry1.url
                #
                # to "url: https://lorry1.url"
                if stripped_line == "url:":
                    repo_content += line.rstrip()
                    merge_url_flag = True
                # convert "tarball" to "raw-file" as lorry2 doesn't support
                # ie: convert
                #
                # type: tarball
                # url: https://lorry1.tar.gz
                #
                # to:
                #
                # type: raw-file
                # urls:
                #   - destination: tarballs
                #     url: https://lorr1.tar.gz
                elif stripped_line.startswith("type: tarball"):
                    repo_content += line.replace("tarball", "raw-file")
                    repo_content += line.replace("type: tarball", "urls:")
                    repo_content += line.replace("type: tarball",
                                                 "  - destination: tarballs")
                    tarball_flag = True
                # drop "type: hg" repo as lorry2 doesn't support
                elif stripped_line.startswith("type: hg"):
                    repo_content = ""
                    new_repo_flag = False
                else:
                    if merge_url_flag:
                        repo_content += f" {stripped_line}\n"
                        merge_url_flag = False
                    elif tarball_flag:
                        repo_content += f"    {line}"
                        tarball_flag = False
                    else:
                        repo_content += line

        if repo_content:
            content += repo_content

    return content


def convert_to_lorry2(lorry_config_repo):
    """
    convert lorry_config_repo to be compatible with lorry2
    """

    dirs_to_ignore = [".git"]
    dir_path = Path(lorry_config_repo).absolute()
    subdirs = sorted([d for d in dir_path.iterdir()
                      if d.is_dir() and d.name not in dirs_to_ignore])

    for subdir in subdirs:
        lorry_files = [
            f for f in subdir.iterdir() if f.is_file() and f.suffix == ".lorry"
        ]

        for lorry_file in lorry_files:
            content = convert_file(lorry_file)
            with open(lorry_file, "w", encoding="utf-8") as w:
                w.write(content)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        usage="""convert_to_lorry2.py lorry_config_repo

        lorry_config_repo: path to lorry config repo that need to be converted
        """,
        description="convert lorry config repo to lorry2 format")
    parser.add_argument('lorry_config_repo')
    args = parser.parse_args()
    convert_to_lorry2(args.lorry_config_repo)
