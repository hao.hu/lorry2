# Lorry 2 End To End Testing

Test framework for running Lorry2 against a gitlab instance to check for API breakage and general usability changes.

These tests uses Robot Framework, combined with kubernetes through [kind][kind] as well as standard kubernetes environment tools to deploy a full gitlab cluster for testing, and automatically deploying lorry against it.

[kind]: https://kind.sigs.k8s.io/

## Requirements

* Python3 (>= 3.10)
* (optional) virtualenv
* docker
* kind
* helm
* kubectl

## Test Setup

To set up for running the tests, assuming you have all the Requirements installed and in your path, run the following:

```bash
# optional virtualenv setup
virtualenv .venv
source .venv/bin/activate

# install dependencies
pip install -r requirements.txt
```

That will install the robotframework and other libraries as needed.

## Running Tests

To run the tests, you can either use robot directly to run the tests as follows:

```bash
robot --outputdir results .
```

or use the wrapper script which handles extra functionality such as setup and teardown skipping:

```bash
./run_test.py --do-setup --do-teardown
```

check the help command (`--help`) for other available options.

### Robot Variables

The following variables are available for setup of the framework:

* `kind_port_external_http` - The externally accessible port for http ingress. Defaults to 80
* `kind_port_external_https` - The externally accessible port for https ingress. Defaults to 443
* `kind_port_external_ssh` - The externally accessible port for ssh ingress. Defaults to 32022
* `helm_gitlab_chart_version` - The chart version to use for deployment of gitlab. Defaults to 7.1.1

These variables can be overriden by passing them in on the command line, for example:

```bash
robot \
    --variable kind_port_external_http:32080
    --outputdir results \
    .
```

They can also be overriden by creating a variable file. See the [Robot Framework Variable Docs][rf-var-docs] for more information.

[rf-var-docs]: https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#variables

There are also some variables available which allow for modifying the setup and teardown stages during development:

* `SKIP_CLUSTER_TEARDOWN`: set to true to leave the kind cluster and gitlab deployment intact
* `SKIP_CLUSTER_SETUP`: set to true to not run the cluster deployment steps. Useful with `SKIP_CLUSTER_TEARDOWN` to speed up iterations
* `SUITE_TMP_DIR`: the tmp dir used for storing kubeconfig and other templated files during suite runtime
* `SUITE_KIND_CLUSTER`: Name of the kind cluster deployed to

General usage of these variables is as follows:

```bash
# disable teardown of the cluster
robot \
    --variable SKIP_CLUSTER_TEARDOWN:True \
    --outputdir results \
    .

# OR using script
./run_test.py --do-setup

# gather SUITE_TMP_DIR and SUITE_KIND_CLUSTER from results, then run again, skipping cluster setup
robot \
    --variable SKIP_CLUSTER_TEARDOWN:True \
    --variable SKIP_CLUSTER_SETUP:True \
    --variable SUITE_TMP_DIR:/tmp/tmp.1Fvme2iRaS \
    --variable SUITE_KIND_CLUSTER:lorry2-e2e-y4dzhqpniquj \
    --outputdir results \
    .

# OR using script
./run_test.py \
    --tmp-dir /tmp/tmp.1Fvme2iRaS \
    --cluster lorry2-e2e-y4dzhqpniquj
```

This removes the initial cluster setup time (approx 3 to 5 minutes at time of writing) for faster iteration of tests

## Todo

* allow for setting `SUITE_TMP_DIR` and `SUITE_KIND_CLUSTER` arbitrarily, and the cluster etc. still being deployed and torn down properly.

* Add lorry2 volumes to testing helm chart


## Useful Links

* [Robot Framework Best Practices](https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst)