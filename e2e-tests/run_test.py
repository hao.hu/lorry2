#! /usr/bin/env python3

import subprocess
import argparse

parser = argparse.ArgumentParser(description='Run the tests')
parser.add_argument('--do-teardown', action='store_true')
parser.add_argument('--do-setup', action='store_true')
parser.add_argument('--tmp-dir')
parser.add_argument('--cluster')

args = parser.parse_args()

print(args)

cmd = [
    'robot',
    '--outputdir', 'results',
]

if not args.do_setup:
    cmd.append('-v')
    cmd.append('SKIP_CLUSTER_SETUP:True')
    if not args.tmp_dir:
        print('tmp-dir required when not using setup')
        exit(1)
    if not args.cluster:
        print('cluster required when not using setup')
        exit(1)

if not args.do_teardown:
    cmd.append('-v')
    cmd.append('SKIP_CLUSTER_TEARDOWN:True')

if args.tmp_dir:
    cmd.append('-v')
    cmd.append(f'SUITE_TMP_DIR:{args.tmp_dir}')

if args.cluster:
    cmd.append('-v')
    cmd.append(f'SUITE_KIND_CLUSTER:{args.cluster}')

cmd.append('.')

print(cmd)

subprocess.run(cmd, check=True)
