*** Settings ***
Library     Process
Library     RequestsLibrary
Resource    default.resource


*** Variables ***
${SUITE_TMP_DIR}            ${None}
${SUITE_KIND_CLUSTER}       ${None}


*** Test Cases ***
Find Kubernetes Services
    ${kubectl_result}    Run Process
    ...    kubectl    get    services    --all-namespaces
    ...    env:KUBECONFIG=${SUITE_TMP_DIR}/kubeconfig
    Log    ${kubectl_result.stdout}

Check Gitlab Responds
    ${response}    GET    http://gitlab.${SUITE_KIND_CLUSTER}.localhost:${kind_port_external_http}
    Status Should Be    200
    Should Contain    ${response.text}    GitLab
