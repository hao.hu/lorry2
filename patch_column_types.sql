-- TODO Should I bunch these together without a seperator? Would taht result in them being treated
ALTER TABLE lorries 
       ADD COLUMN temp_last_run_exit INT;

UPDATE lorries 
       SET temp_last_run_exit = CAST(last_run_exit as INT);

ALTER TABLE lorries DROP COLUMN last_run_exit;

ALTER TABLE lorries RENAME COLUMN temp_last_run_exit TO last_run_exit;

ALTER TABLE lorries 
       ADD COLUMN temp_last_run_error TEXT;

UPDATE lorries 
       SET temp_last_run_error = CAST(last_run_error as TEXT);

ALTER TABLE lorries DROP COLUMN last_run_error;

ALTER TABLE lorries RENAME COLUMN temp_last_run_error TO last_run_error;


    