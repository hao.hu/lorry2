use logging::debug;
use std::{
    fs::File,
    io::Write,
    path::{Path, PathBuf},
};
use thiserror::Error;

use crate::Arguments;

pub(crate) fn should_check_certificates(
    global_check_certificates: bool,
    per_lorry_check_certificates: bool,
) -> bool {
    global_check_certificates && per_lorry_check_certificates
}

pub(crate) fn missing_or_empty(file_destination: &Path) -> Result<bool, std::io::Error> {
    Ok(!file_destination.is_file() || file_destination.metadata()?.len() == 0)
}

#[derive(Error, Debug)]
pub enum FileDownloadError {
    #[error("Could not open the file")]
    CouldNotOpenForWriting {
        source: std::io::Error,
        target: PathBuf,
    },
    #[error("Error occurred while setting up CURL")]
    CouldNotSetUpDownload { source: curl::Error, url: String },

    #[error("There was an error during the download itself")]
    DownloadFailed { source: curl::Error, url: String },
}

///download the file from `url` to `file_destination`.
pub(crate) async fn download_file(
    file_destination: &Path,
    url: &url::Url,
    check_certs: bool,
    arguments: &Arguments,
) -> Result<(), FileDownloadError> {
    let mut file_to_write_to =
        File::create(file_destination).map_err(|e| FileDownloadError::CouldNotOpenForWriting {
            source: e,
            target: file_destination.to_path_buf(),
        })?;

    let mut handle = curl::easy::Easy::new(); //TODO look into making this async
    let mut send_headers = curl::easy::List::new();
    let mut last_modified_recv_header = None;
    handle
        .url(url.as_str())
        .and_then(|_| {
            send_headers.append("User-Agent: Lorry <TODO PUT VERSION AND REPO INFO HERE>")
        })
        .and_then(|_| handle.http_headers(send_headers))
        .and_then(|_| handle.fail_on_error(true))
        .and_then(|_| {
            handle.ssl_verify_peer(should_check_certificates(
                arguments.check_ssl_certificates,
                check_certs,
            ))
        })
        .and_then(|_| {
            handle.ssl_verify_host(should_check_certificates(
                arguments.check_ssl_certificates,
                check_certs,
            ))
        })
        .and_then(|_| {
            handle.write_function(
                move |data| file_to_write_to.write(data).or(Ok(0)), //This causes the function to signal an error becuase 0 bytes have been written.
            )
        })
        .map_err(|e| FileDownloadError::CouldNotSetUpDownload {
            source: e,
            url: url.to_string(),
        })?;

    {
        let mut transfer = handle.transfer();

        //TODO would like to move this to the and_then() chain above but it causes a nightmare of lifetime errors
        transfer
            .header_function(|header| {
                let header_line = match std::str::from_utf8(header) {
                    Ok(s) => s,
                    Err(_) => return false,
                };
                if let Some(s) = header_line.to_lowercase().strip_prefix("last-modified: ") {
                    last_modified_recv_header = Some(s.to_owned());
                }
                true
            })
            .map_err(|e| FileDownloadError::CouldNotSetUpDownload {
                source: e,
                url: url.to_string(),
            })?;

        transfer
            .perform()
            .map_err(|e| FileDownloadError::DownloadFailed {
                source: e,
                url: url.to_string(),
            })?;
    }

    let file_destination_as_string = file_destination.to_string_lossy();
    //get the last modified time as UTC

    let url_date = match last_modified_recv_header{
        Some(s) => async{
            match chrono::DateTime::parse_from_rfc2822(s.trim()) {
                Ok(dt) => Some(dt),
                Err(e) => {async {
                    debug(&format!(
                    "Was unable to parse the Last-Modified header from {url} into a timestamp. This is not a critical error but will result in incorrect access time metadata for {file_destination_as_string}. Error: {e}
                    the header was : {s}"
                ),
                &arguments.transmitter).await;
                None
            }.await
            }
        }
        }.await,
        None => None,
    }.map(chrono::DateTime::<chrono::Utc>::from);

    //if we could extract the remote file's last modified date, apply it to the local file
    match url_date {
        None => Ok(()),
        Some(dt) => {
            async {
                let t =
                    filetime::FileTime::from_unix_time(dt.timestamp(), dt.timestamp_subsec_nanos());
                match filetime::set_file_times(file_destination, t, t) {
                    Err(e) => {
                        debug(&format!(
                    "Was unable to set the last modified time from {url} to to file at {file_destination_as_string}. This is not a critical error but will result in incorrect access time metadata. Error: {e}"
                ),
                &arguments.transmitter).await;
                        Ok(())
                    } //discard an error here; it doesn't matter.

                    Ok(()) => Ok(()),
                }
            }.await
        }
    }
}
