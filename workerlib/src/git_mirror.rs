use thiserror::Error;

use crate::{Arguments, InternalGitDirectory};
use utils::command_wrapper::{CommandBuilder, CommandExecutionError};

#[derive(Error, Debug)]
pub enum GitMirrorError {
    /// The git fetch command to the updtream folder failed. The results of the command execution are contained.
    #[error("Could not fetch from upstream")]
    CouldNotFetch {
        source: CommandExecutionError,
        upstream: String,
    },
}
/// mirrors a git repo by pulling down the upstream.
pub(crate) async fn fetch_git_repo(
    git_repo: &InternalGitDirectory,
    upstream_url: &str,
    args: &Arguments,
    check_certificates: bool,
) -> Result<(), GitMirrorError> {
    {
        let debug = |s: String| async move { logging::debug(&s, &args.transmitter).await };

        let mut c = CommandBuilder::new("git").args(&[
            "-c",
            "gc.autodetach=false",
            "fetch",
            "--prune",
            upstream_url,
            "+refs/heads/*:refs/heads/*",
            "+refs/tags/*:refs/tags/*",
        ]);

        if !check_certificates {
            c = c.env(&[("GIT_SSL_NO_VERIFY", "true")]);
        }

        c.execute(git_repo, debug)
    }
    .await
    .map_err(|e| GitMirrorError::CouldNotFetch {
        source: e,
        upstream: upstream_url.to_owned(),
    })?;
    Ok(())
}
