use std::{
    fs,
    io::Read,
    path::{Path, PathBuf},
};

use crate::Arguments;
use logging::progress;
use thiserror::Error;

use utils::command_wrapper::CommandBuilder;

use super::{EnsureGitRepoError, InternalGitDirectory, COUNT_FILE_NAME};

use super::{GitObjectsFolder, Workspace};

mod sync_internals;

#[derive(Error, Debug)]
pub enum WorkspaceSetupError {
    /// There was an error accessing the file storing the update count of one fo the internal repositories.
    #[error("Could not read the update count file for the A/B switched repo")]
    CouldNotReadUpdateCountFile {
        path: PathBuf,
        source: std::io::Error,
    },
    /// Tried to rename the out-of-date folder, to keep as a backup. This operation failed.
    #[error("Tried to take a backup of the folder to be written over, but failed")]
    CouldNotTakeBackup {
        source: std::io::Error,
        from: InternalGitDirectory,
        backup_dir: PathBuf,
    },

    /// There was an error when trying to delete the out-of-date repo.
    #[error("Tried to delete older directory in preperation for overwriting, but failed")]
    CouldNotDeleteTemporaryDir {
        source: std::io::Error,
        dir: InternalGitDirectory,
    },

    /// An error occurred when we were copying the old, stable, repository into the space for the working directory.
    /// The contained Error should give a more detailed explanation of what caused the error.
    #[error("Could not copy the old folder over the temporary repo")]
    CouldNotCopyGitDir {
        source: sync_internals::CopyGitDirFailure,
        old: InternalGitDirectory,
        temp: InternalGitDirectory,
    },

    /// The update count file was read, but its contents are not an integer.
    #[error("Could not parse the update count file")]
    CouldNotGetUpdateCount {
        path: PathBuf,
        source: std::num::ParseIntError,
    },

    /// There was a file access error trying to access the update count file.
    #[error("Could not access the update count file")]
    CouldNotOpenCountFile {
        source: std::io::Error,
        path: PathBuf,
    },

    /// When trying to ascertain which internal repo is more recent, we read the updaet count file's "last modified" timestamp.
    /// However, there was an error trying to read the timestamp.
    #[error("Could not access the update count file's metadata")]
    CouldNotGetMetaData {
        source: std::io::Error,
        path: PathBuf,
    },

    /// Failed to create a folder to contain the working area of this mirror.
    #[error("Error while creating workspace folder")]
    CouldNotCreateWorkspaceFolder {
        source: std::io::Error,
        path: PathBuf,
    },

    /// The internal working area was a simple git repo, possibly becuase the user recently migrated to using lorry.
    /// However, there was a problem when converting the working area to use lorry's A/B switching layout.
    #[error("Failed to migrate repo")]
    MigrationFailed(MigrationFailure),

    /// The specified backup policy does not allow for more backup folders, but there was a problem removing the old backups.
    #[error("Had to delete old backup dir, but was unable to do so")]
    CouldNotDeleteOldBackups {
        source: std::io::Error,
        path: PathBuf,
    },

    /// We need to make sure that there exists a git repository for the mirroring operation to work on.
    /// There was an error creating this repository.
    #[error(
        "Encountered an error making sure that the temportary internal repository to operate on."
    )]
    CouldNotEnsureTempRepo(EnsureGitRepoError),
}

/// Figures out which repos in the directory are the temporary and active directory.
/// Will then either delete or back up the temporary, and copy the current active directory over it.
/// Now that the workspace has been set up, returns the corresponding `Workspace` object representing the workspace state.
/// Should there be some failure during the mirroring process, the active(i.e garuanteed up-to-date) repo will be untouched.
/// Once operations are done, the "temporary" becomes the active as it is more up to date, and will be pushed to the downstream if applicable.
pub(crate) async fn prepare_repos<'a>(
    lorry_name: &'a str,
    arguments: &Arguments,
) -> Result<Workspace<'a>, WorkspaceSetupError> {
    //get the name of the local directory
    let local_dir = arguments.working_area.join(lorry_name.replace('/', "_"));
    fs::create_dir_all(&local_dir).map_err(|e| {
        WorkspaceSetupError::CouldNotCreateWorkspaceFolder {
            source: e,
            path: local_dir.clone(),
        }
    })?;

    //migrate repo if it does not have A/B switching set up yet
    migrate_repos(&local_dir, arguments)
        .await
        .map_err(WorkspaceSetupError::MigrationFailed)?;

    let extract_repo_data = |p: PathBuf| -> Result<
        (Option<(u32, std::time::SystemTime)>, PathBuf),
        WorkspaceSetupError,
    > {
        let count_file = p.join(super::COUNT_FILE_NAME);
        if count_file.exists() {
            let mut f = fs::File::open(&count_file).map_err(|e| {
                WorkspaceSetupError::CouldNotOpenCountFile {
                    source: e,
                    path: count_file.clone(),
                }
            })?;
            let timestamp_last_modified = f.metadata().and_then(|m| m.modified()).map_err(|e| {
                WorkspaceSetupError::CouldNotGetMetaData {
                    source: e,
                    path: count_file.clone(),
                }
            })?;

            let text = &mut String::new();
            let count = f
                .read_to_string(text)
                .map_err(|e| WorkspaceSetupError::CouldNotReadUpdateCountFile {
                    path: count_file.clone(),
                    source: e,
                })
                .and_then(|_| {
                    text.trim().parse::<u32>().map_err(|e| {
                        WorkspaceSetupError::CouldNotGetUpdateCount {
                            source: e,
                            path: count_file.clone(),
                        }
                    })
                })?;

            Ok((Some((count, timestamp_last_modified)), p))
        } else {
            Ok((None, p)) //return dummy value as the update count file does not exist
        }
    };
    let a_dir = extract_repo_data(local_dir.join("git-a"))?;

    let b_dir = extract_repo_data(local_dir.join("git-b"))?;

    //TODO system times cannot necessarily be sorted on according to the docs, so is the sort the best idea?
    let mut in_order = [a_dir, b_dir];
    in_order.sort();
    let [temp_repo, active_repo] = in_order;

    let (temp_repo_path, active_repo_count, active_repo_timestamp, active_repo_path) =
        match temp_repo {
            (Some((_, _)), temp_repo_path) => (
                InternalGitDirectory(temp_repo_path),
                active_repo.0.expect("sort in prepare_repos did not work").0,
                active_repo.0.expect("sort in prepare_repos did not work").1,
                InternalGitDirectory(active_repo.1),
            ),
            (None, temp_repo_path) => {
                //The temp repo had no update count file.
                //thus, we can delete it in prperation for overwiting

                if active_repo.1.exists() {
                    (
                        //move along so that we copy active over temp later
                        InternalGitDirectory(temp_repo_path),
                        active_repo.0.expect("sort in prepare_repos did not work").0,
                        active_repo.0.expect("sort in prepare_repos did not work").1,
                        InternalGitDirectory(active_repo.1),
                    )
                } else {
                    //neither repo exists, return early

                    let post_fail_backup_dir = get_backup_dir(&local_dir, arguments)?;

                    //create the temp repo so that there will be something to work on
                    let temp_repo_path = InternalGitDirectory(temp_repo_path);
                    temp_repo_path
                        .ensure_exists(arguments.verbose_logging, &arguments.transmitter)
                        .await
                        .map_err(WorkspaceSetupError::CouldNotEnsureTempRepo)?;
                    return Ok(Workspace {
                        name: lorry_name,
                        path: local_dir,
                        temp_repo: temp_repo_path,
                        active_repo: InternalGitDirectory(active_repo.1),
                        next_update_count: 1,
                        backup_repo: post_fail_backup_dir,
                    });
                }
            }
        };

    sync_internals::update_temporary_repo(
        &temp_repo_path,
        arguments,
        active_repo_timestamp,
        &active_repo_path,
        &local_dir,
    )?;

    let post_fail_backup_dir = get_backup_dir(&local_dir, arguments)?;
    temp_repo_path
        .ensure_exists(arguments.verbose_logging, &arguments.transmitter)
        .await
        .map_err(WorkspaceSetupError::CouldNotEnsureTempRepo)?;
    Ok(Workspace {
        name: lorry_name,
        path: local_dir,
        temp_repo: temp_repo_path,
        active_repo: active_repo_path,
        next_update_count: active_repo_count + 1,
        backup_repo: post_fail_backup_dir,
    })
}

/// Figures out the directory that will be used to hold the temporary repositoyr should this run fail.
/// Also deletes any previous backups if our backup policy so dictates.
fn get_backup_dir(local_dir: &Path, arguments: &Arguments) -> Result<PathBuf, WorkspaceSetupError> {
    let post_fail_backup_dir = local_dir.join(if arguments.keep_multiple_backups {
        format!(
            "git-post-fail-{}",
            chrono::offset::Local::now().format("%F-%T")
        )
    } else {
        "git-post-fail".to_string()
    });
    if !arguments.keep_multiple_backups && post_fail_backup_dir.exists() {
        fs::remove_dir_all(&post_fail_backup_dir).map_err(|e| {
            WorkspaceSetupError::CouldNotDeleteOldBackups {
                source: e,
                path: post_fail_backup_dir.clone(),
            }
        })?;
    }
    Ok(post_fail_backup_dir)
}

#[derive(Error, Debug)]
pub enum MigrationFailure {
    #[error("Could not write to update count file")]
    WriteUpdateFile {
        source: std::io::Error,
        path: PathBuf,
    },
    #[error("Could not set config options for the migrated git repo")]
    WriteConfig {
        source: utils::command_wrapper::CommandExecutionError,
        path: PathBuf,
    },
    #[error("Could not remove the old repository")]
    CleanUp {
        source: std::io::Error,
        folder: PathBuf,
    },
}

/// Check that the repo at this path is not in A/B switched form; if not, convert it to that form and delete the old repo.
/// This is important for users who are swithing to lorry from another system (or a *very* old version of lorry...)
pub(crate) async fn migrate_repos(
    local_dirname: &Path,
    arguments: &Arguments,
) -> Result<(), MigrationFailure> {
    let debug = |s: String| async move { logging::debug(&s, &arguments.transmitter).await };

    let old_repo = local_dirname.join("git");
    if old_repo.exists() {
        //migrate this repo to A/B form
        let new_repo = local_dirname.join("git-a");
        if new_repo.exists() {
            //this repo is already migrated, but the old version still exists.
            progress(format!("You have an old-style and a A/B switched copy of the repository  at {}, consider deleting the non-switching version.",local_dirname.display()), arguments.verbose_logging,&arguments.transmitter).await
        } else {
            let old_git_dir = {
                let candidate = old_repo.join(".git");
                if candidate.exists() {
                    candidate
                } else {
                    old_repo
                }
            };
            CommandBuilder::new("git")
                .args(&["config", "core.bare", "true"])
                .execute(&old_git_dir, debug)
                .await
                .map_err(|e| MigrationFailure::WriteConfig {
                    source: e,
                    path: old_git_dir.clone(),
                })?;

            CommandBuilder::new("git")
                .args(&["config", "core.logallrefupdates", "false"])
                .execute(&old_git_dir, debug)
                .await
                .map_err(|e| MigrationFailure::WriteConfig {
                    source: e,
                    path: old_git_dir.clone(),
                })?;

            //create an update count file for the new git repo
            let count_file_path = old_git_dir.join(COUNT_FILE_NAME);
            fs::write(&count_file_path, "1\n").map_err(|e| MigrationFailure::WriteUpdateFile {
                source: e,
                path: count_file_path,
            })?;
        }
    }

    //delete the old backup repo - we shouldn't need it anymore.
    let old_bkup = local_dirname.join("git-pre-update");
    if old_bkup.exists() {
        fs::remove_dir_all(&old_bkup).map_err(|e| MigrationFailure::CleanUp {
            source: e,
            folder: old_bkup.clone(),
        })?;
    }
    Ok(())
}
