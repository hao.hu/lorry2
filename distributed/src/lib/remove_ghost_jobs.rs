// It is potentially possible that a worker stop working on a job and not notify the controller
// This could happen due to a crash, or the worker being forcibly stopped.
// This will result in the lorry being marked as in progress in the database, blocking them from being mirrored again
// because the controller will be unable to issue the approprate job
// If this occurs, the administrator should call this endpoint, which will scan for any suspiciously long-running jobs
// and mark them as un-run, allowing them to be issued again

//TODO call this on startup; when we are started there is the possibility a job was running when the whole system was
// previously shut down, which would leave a job as a ghost

use super::comms::JobId;
use crate::state_db::StateDatabase;
use crate::{comms, ControllerSettings};
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use thiserror::Error;
use tokio::time::{sleep_until, Instant};
use tracing::{error, info};

/// Periodically remove ghost jobs
#[tracing::instrument(skip_all)]
pub(crate) async fn periodic(pool: Arc<StateDatabase>, app_settings: &ControllerSettings) {
    loop {
        let start = Instant::now();
        match remove_ghost_jobs(pool.clone(), app_settings).await {
            Ok(res) => info!("Attempted to remove ghosts: {res:#?}"),
            Err(err) => error!("Failed to remove ghosts: {err}"),
        };
        sleep_until(start + app_settings.remove_ghost_period).await;
    }
}

#[tracing::instrument(skip_all)]
pub(crate) async fn remove_ghost_jobs(
    db: Arc<StateDatabase>,
    _app_settings: &ControllerSettings,
) -> Result<GhostBustingResults, GhostBustingError> {
    let running_jobs = db.get_running_jobs().await?;

    let current_time: super::comms::TimeStamp = db.get_current_time().await;

    let (mut ghost_jobs, mut failed_ghosts, mut uncertain_jobs) = (
        Vec::<super::state_db::JobRow>::new(),
        Vec::<FailedJob>::new(),
        Vec::<UnknownJob>::new(),
    );

    for job_id in running_jobs {
        let job_info = db.get_job_info(job_id).await;
        match job_info {
            Ok(job_info) => {
                let last_updated = job_info.updated();
                let lorry_info = db.get_lorry_info(job_info.path()).await;

                match lorry_info {
                    Ok(lorry_info) => {
                        let ghost_timout: super::comms::Interval = lorry_info.interval * 2; // I figure this is a good default; if a job has handged so long it has missed two mirroing cycles, it should be safe to assume it is a ghost job
                        let time_since_update: super::comms::Interval = current_time - last_updated;
                        if time_since_update > ghost_timout {
                            ghost_jobs.push(job_info)
                        }
                    }
                    Err(e) => uncertain_jobs.push(UnknownJob {
                        id: job_id,
                        path: Some(job_info.path().clone()),
                        err: format!("{e:?}"),
                    }),
                }
            }
            Err(e) => uncertain_jobs.push(UnknownJob {
                id: job_id,
                path: None,
                err: format!("{e:?}"),
            }),
        }
    }

    let mut succeeded_ghost_jobs = Vec::<StoppedJob>::new();
    for ghost_job in ghost_jobs {
        if let Err(e) = db.set_kill_job(ghost_job.job_id(), true).await {
            failed_ghosts.push(FailedJob {
                id: ghost_job.job_id(),
                path: ghost_job.path().clone(),
                err: format!("{e:?}"),
            });
            continue;
        }

        db.append_to_job_output(ghost_job.job_id(), "\nTERMINATED DUE TO GHOST TIMEOUT")
            .await
            .ok(); // Continue working if this fails becuase this is just a nicety and not necessary for function

        let set_exit_status = db
            .set_job_exit(
                ghost_job.job_id(),
                super::comms::JobFinishedResults {
                    exit_code: 127,
                    disk_usage: super::comms::DiskUsage::new(-1),
                },
                current_time,
            )
            .await;

        let set_job_finished = db.set_running_job(ghost_job.path(), None).await;

        if let Err(e) = set_exit_status.and(set_job_finished) {
            failed_ghosts.push(FailedJob {
                id: ghost_job.job_id(),
                path: ghost_job.path().clone(),
                err: format!("{e:?}"),
            });
            continue;
        }

        tracing::info!(
            "Successfully halted ghost job {} ({})",
            ghost_job.job_id(),
            ghost_job.path().to_string()
        );

        succeeded_ghost_jobs.push(StoppedJob {
            id: ghost_job.job_id(),
            path: ghost_job.path().clone(),
        })
    }

    Ok(GhostBustingResults {
        stopped_ghosts: succeeded_ghost_jobs,
        failed_to_stop: failed_ghosts,
        failed_to_check: uncertain_jobs,
    })
}

#[derive(Error, Debug)]
pub(crate) enum GhostBustingError {
    #[error("There was an error trying to find what jobs are currently running: {0}")]
    CouldNotGetRunningJobs(#[from] sqlx::Error),
}

/// Struct containing information about the results of running the remove_ghost_jobs operation.
#[derive(Debug, Serialize, Deserialize)]
pub struct GhostBustingResults {
    /// Ghost jobs that were successfully stopped
    pub stopped_ghosts: Vec<StoppedJob>,
    /// Ghost jobs that were not successfully stopped
    pub failed_to_stop: Vec<FailedJob>,
    /// Jobs that we could not ascertain if they were ghosts or not
    pub failed_to_check: Vec<UnknownJob>,
} //TODO is stopped_ghosts is empty, shouldn't that be reported as an error...?

/// TODO these three are similar enough I'm sure they can be merged together in some way

/// Details about ghost jobs that were detected and marked as no longer running.
#[derive(Debug, Serialize, Deserialize)]
pub struct StoppedJob {
    pub id: JobId,
    /// The lorry the job was supposed to mirror
    path: comms::LorryPath,
}

/// Details about jobs that were detected as ghosts but could not be marked as not running for whatever reason.
#[derive(Serialize, Debug, Deserialize)]
pub struct FailedJob {
    id: JobId,
    /// The repository the job was supposed to mirror
    path: comms::LorryPath,
    /// Error message that occured when trying to stop the job
    err: String,
}

/// Jobs that were found that are mirroring some repository that the controller does not know about.
#[derive(Debug, Serialize, Deserialize)]
pub struct UnknownJob {
    id: JobId,
    path: Option<comms::LorryPath>,
    err: String,
}
