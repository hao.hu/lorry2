//TODO the way this is implemented, it might make more sense to use a Trait Object instead of an enum?

use crate::comms;
use crate::hosts;
use tracing::warn;
use url::Url;

/// Connection to a downstream host.
#[derive(Clone, Debug)]
pub enum Downstream {
    GitLab(hosts::gitlab::GitLabDownstream),
    Local(hosts::local::LocalDownstream),
}

impl Downstream {
    /// Create the target git repository for a minion to mirror to.
    pub async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: RepoMetadata,
    ) -> Result<(), Box<dyn std::error::Error + Send>> {
        match self {
            Downstream::GitLab(g) => g.prepare_repo(repo_path, metadata).await,
            Downstream::Local(l) => l.prepare_repo(repo_path, metadata).await,
        }
    }

    pub(crate) fn mirror_base_url(&self) -> Url {
        match self {
            Downstream::GitLab(g) => g.git_url.clone(),
            // Don't care out the panic here - `Local` is only used for testing purposes!
            Downstream::Local(l) => Url::from_directory_path(&l.base_dir)
                .expect("Lorry needs an absolute file path for local downstreams"),
        }
    }

    pub(crate) fn insecure(&self) -> Option<bool> {
        match self {
            Downstream::GitLab(g) => Some(g.insecure),
            Downstream::Local(_) => None,
        }
    }
}

/// Configuration for the connection to the downstream host. Can be converted to a `Downstream` to create the connection.
#[derive(Debug)]
pub enum DownstreamSetup {
    /// Use the local filesystem to host mirror repositories.
    Local {
        /// The directory to the folder containing mirrors
        base_dir: std::path::PathBuf,
    },
    /// Use a downstream git server running GitLab
    Gitlab {
        /// The base host name of the server.
        /// Please do not include a scheme identifier.
        hostname: Url,

        /// Visibility setting for the created repositories
        downstream_visibility: Visibility,

        /// Private access token to use for access to the GitLab API. Make sure that this has full access to the server you are mirroring to!
        private_token: String,

        /// Permit insecure access to gitlab
        insecure: bool,
    },
}

//TODO perhaps clap::Parser will make this more consistent wrt how it is configured? i.e Make file and CLI the same
#[derive(clap::Subcommand, Debug, serde::Deserialize)]
#[serde(tag = "downstream")]
pub enum PartialDownstreamSetup {
    /// We don't want end-users to be able to select this option, it exists purely for testing purposes.
    #[command(skip)]
    #[serde(skip)]
    Local {
        base_dir: Option<std::path::PathBuf>,
    },
    /// Use a downstream git server running GitLab to host mirrors.
    #[command(name = "gitlab")]
    #[serde(rename = "gitlab")]
    Gitlab {
        /// The hostname of the server.
        /// Please do not include a scheme identifier.
        #[arg(long = "hostname")]
        #[serde(rename = "hostname")]
        hostname: Option<String>,

        /// Visibility setting for the created repositories
        #[arg(long = "visibility", value_enum)]
        #[serde(rename = "visibility")]
        downstream_visibility: Option<Visibility>,

        /// Private access token to use for access to the GitLab API. Make sure that this has full access to the server you are mirroring to!
        #[arg(long = "gitlab-private-token")]
        #[serde(rename = "gitlab-private-token")]
        private_token: Option<String>,

        /// Permit insecure access to gitlab (ie http rather than https)
        #[arg(long = "gitlab-insecure-http")]
        #[serde(rename = "gitlab-insecure-http")]
        insecure: Option<bool>,
    },
}

#[derive(thiserror::Error, Debug)]
pub enum DownstreamSetupError {
    #[error("Local downstream was supplied no directory")]
    NoBaseDir,

    #[error("Remote downstream hostname not provided")]
    MissingHostname,

    #[error(
        "Invalid downstream hostname {0}.
If you have included a scheme/protocol: please remove it.
If you have specified a path: lorry cannot currently handle this.
Please open an issue at <https://gitlab.com/CodethinkLabs/lorry/lorry2/-/issues>.
"
    )]
    InvalidHostname(String),

    #[error("Remote downstream has not been given a valid visibility option for its mirrors")]
    InvalidVisibility,

    #[error("Remote downstream was not supplied an access token")]
    NoToken,
}

impl TryFrom<PartialDownstreamSetup> for DownstreamSetup {
    type Error = DownstreamSetupError;

    fn try_from(value: PartialDownstreamSetup) -> Result<Self, Self::Error> {
        match value {
            PartialDownstreamSetup::Local { base_dir } => Ok(Self::Local {
                base_dir: base_dir.ok_or(DownstreamSetupError::NoBaseDir)?,
            }),
            PartialDownstreamSetup::Gitlab {
                hostname,
                downstream_visibility,
                private_token,
                insecure,
            } => {
                let hostname = hostname.ok_or(DownstreamSetupError::MissingHostname)?;
                let mut downstream_url = Url::parse(&format!("https://{hostname}"))
                    .map_err(|_| DownstreamSetupError::InvalidHostname(hostname.clone()))?;
                if downstream_url.path() != "/" {
                    return Err(DownstreamSetupError::InvalidHostname(hostname));
                }
                if downstream_url.username() != "" || downstream_url.password().is_some() {
                    warn!(
                        "Lorry will ignore the authentication credentials provided in 'hostname': {}",
                        downstream_url.authority()
                    );
                    downstream_url.set_password(None).unwrap();
                    downstream_url.set_username("").unwrap();
                }

                Ok(Self::Gitlab {
                    hostname: downstream_url,
                    downstream_visibility: downstream_visibility
                        .ok_or(DownstreamSetupError::InvalidVisibility)?,
                    private_token: private_token.ok_or(DownstreamSetupError::NoToken)?,
                    insecure: insecure.unwrap_or(false),
                })
            }
        }
    }
}

/// Visibility setting to use when creating a repo. While this is host-dependent,
/// the current implementation should be lowest common denominator.
#[derive(clap::ValueEnum, Copy, Clone, Debug, serde::Deserialize)]
pub enum Visibility {
    #[serde(rename = "public")]
    Public,

    #[serde(rename = "internal")]
    Internal,

    #[serde(rename = "private")]
    Private,
}

#[async_convert::async_trait]
impl async_convert::TryFrom<DownstreamSetup> for Downstream {
    type Error = gitlab::GitlabError;

    async fn try_from(value: DownstreamSetup) -> Result<Self, Self::Error> {
        match value {
            DownstreamSetup::Local { base_dir } => {
                Ok(Downstream::Local(hosts::local::LocalDownstream {
                    base_dir,
                }))
            }
            DownstreamSetup::Gitlab {
                hostname,
                downstream_visibility,
                private_token,
                insecure,
            } => Ok(Downstream::GitLab(hosts::gitlab::GitLabDownstream {
                downstream_visibility: downstream_visibility.into(),
                gitlab_instance: if insecure {
                    // Use `authority` here rather than `host_str` to include port specification
                    // Any username and password should've been removed earlier by lorry
                    gitlab::GitlabBuilder::new(hostname.authority(), private_token)
                        .insecure()
                        .build_async()
                        .await?
                } else {
                    gitlab::GitlabBuilder::new(hostname.authority(), private_token)
                        .build_async()
                        .await?
                },
                git_url: hostname,
                insecure,
            })),
        }
    }
}

impl From<Visibility> for gitlab::api::common::VisibilityLevel {
    fn from(value: Visibility) -> Self {
        match value {
            Visibility::Public => gitlab::api::common::VisibilityLevel::Public,
            Visibility::Internal => gitlab::api::common::VisibilityLevel::Internal,
            Visibility::Private => gitlab::api::common::VisibilityLevel::Private,
        }
    }
}

/// Metadata about the mirrored repo. Should be reasonably lowest common denominator
#[derive(Debug)]
pub struct RepoMetadata {
    /// The branch to use as the default branch of the repo
    pub head: Option<String>,
    /// Repo description to display to viewers
    pub description: Option<String>,
}
