use std::{collections::BTreeMap, path::PathBuf, sync::Arc, time::Duration};

use axum::{
    routing::{get, post},
    Router,
};

use tokio::{select, spawn};
use tracing::{debug, error, info};
use tracing_subscriber::{prelude::*, EnvFilter, Layer};

use sqlx::migrate::MigrateDatabase;

/// Operations for the backing database
mod state_db;

/// Send mirror specifications to minion instances to carry out.
mod give_job;

/// Receive updates on how a job is going, including receiving logs from the minion program denoting mirror status.
mod update_lorry_status;

//TODO make these pub(crate) instead and figure out which types specifically need to be made public
/// Common types used for communications between controller and minion.
pub mod comms;

/// Link to downstream mirror hosts. Used to manage the hosts, for example, creating empty repositories to mirror to.
pub mod downstream;

/// Specific implementations of downstream operations for different downstream hosts.
pub mod hosts;

/// Give a list of all jobs that have been or are running, as well as status information on them.
pub mod list_jobs;

/// Give a list of all lorries, as well as status information, such as the last run results, or the mirroring interval.
pub mod list_lorries;

/// Program that requests work from the controller application and carries out mirroring tasks appropriately.
pub mod minion;

/// Tell minions that check in to delete old copies of a mirror, because it is believed that corruption or other unrecoverable error occurred.
pub mod purge_repo;

/// Update the controller's configuration by reading from the external configuration repository.
pub mod read_config; //TODO it's only pub so we can test, find a way to make this non-pub again

/// Mark jobs that have been running for too long as not running, to allow them to be scheduled again.
pub mod remove_ghost_jobs;

/// Public endpoint to view errors that have occurred
pub mod report_errors;

pub mod health_check;

use comms::{Communication, TimeStamp};

/// Concatenation of the major sub-components of the controller.
pub struct ControllerComponents {
    /// The server that listens on endpoints to manage minion instances.
    pub router: Router,
    /// A tracing subscriber that sends errors and warnings to `writer` to be written to the database.
    pub logger: Box<dyn tracing::Subscriber + Send + Sync>,
    /// A task that runs in parallel with `router` that accepts error messages from `logger_lifetime`
    /// and logs them to the database.
    pub writer: tokio::task::JoinHandle<()>,
}

//TODO should DB setup be here? perhaps passed in? This breaking up just really exists for testing puposes anyway...
/// Constructs the components of a fully-functioning controller system.
/// * `db_url` - connect to a database at this location as the backing store. Currently, this can only handle SQLite database files.
/// * `app_settings` - a struct containing the parameters of the server. Note this is not a configuration of the mirrors;
/// rather, it is the data necessary to fetch the mirroring configuration later.
pub async fn app(db_url: &str, app_settings: ControllerSettings) -> ControllerComponents {
    if !sqlx::Sqlite::database_exists(db_url).await.unwrap() {
        sqlx::Sqlite::create_database(db_url).await.unwrap();
    }

    let pool = state_db::StateDatabase::connect(db_url)
        .await
        .expect("Could not connect to the database");

    let pool = Arc::new(pool);

    info!("finished connecting to DB");
    let (writer_handle, tracer_lifetime) = create_logger(pool.clone());

    let periodics = spawn({
        let pool = pool.clone();
        let app_settings = app_settings.clone();
        async move {
            select! {
                _ = read_config::periodic(pool.clone(), &app_settings) => {
                    debug!("Periodic read of config stopped for some reason, restarting now!");
                }
                _ = remove_ghost_jobs::periodic(pool, &app_settings) => {
                    debug!("Periodic removal of ghost jobs stopped for some reason, restarting now!");
                }
            }
        }
    });
    spawn(async move {
        if periodics.await.is_err() {
            // Thread panicked or was cancelled
            error!("Lorry is no longer auto-reading confgit or auto-removing ghost jobs");
        }
    });

    //TODO should the POST endpoints perhaps be pub rather than pub(crate)? That way, their argument list would be made public in the docs
    let router = Router::new()
        .route(
            comms::StatusUpdate::PATH,
            post(update_lorry_status::update_lorry_status),
        )
        .route(comms::JobRequest::PATH, post(give_job::give_job_to_worker))
        .route(list_lorries::PATH, get(list_lorries::list_all_lorries))
        .route(list_jobs::PATH, get(list_jobs::list_all_jobs))
        .route(purge_repo::PATH, post(purge_repo::purge_repo))
        .route(report_errors::PATH, get(report_errors::report_errors))
        .route(health_check::PATH, get(health_check::health_check))
        .with_state((pool, app_settings));

    ControllerComponents {
        router,
        writer: writer_handle,
        logger: tracer_lifetime,
    }

    //TODO shouldn't I change the 1.0's to 2.0's, given this is, well, Lorry 2? Or even drop them all together?
}

/// Settings for the creating the lorry controller server.
#[derive(Clone, Debug)]
pub struct ControllerSettings {
    /// Representation of some connection to the host of the downstream git mirrors. This could be git forge instance, a local filesystem, or something else.
    /// This is responsible for creating the downstream git mirrors for worker instances to mirror to.
    pub downstream: downstream::Downstream, //TODO as it stands, we open a session (say, of a gitlab connection) and hold it while the program runs, rather than only opening and closing when needed. Is this the best practice to follow?

    /// When the server updates its configuration, it does so by mirroring the config repository into this folder.
    pub configuration_directory: PathBuf,

    /// Git URL/path pointing to a git repository containing the server configuration. When instructed to update configuration, the server will
    /// git pull from the git repository located here.
    pub confgit_url: String, //TODO make it a URL type? While I don't think anyone is using a local filesystem config in production, it *is* useful for testing to allow for local repos.

    /// Inspect the branch with this name when pulling in `confgit_url` for configuration files.
    pub confgit_branch: String,

    pub confgit_update_period: Duration,

    pub remove_ghost_period: Duration,
}

//TODO place this in some common functions module
/// This is used so we can return sqlx::Error s wrapped nicely as a JSON value in our errors.
pub(crate) fn dump_debug<S>(val: impl std::fmt::Debug, s: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    s.serialize_str(&format!("{:?}", val))
}

//TODO perhaps more of the error logging functionality should be moved to report_errors?
fn create_logger(
    db: Arc<state_db::StateDatabase>,
) -> (
    tokio::task::JoinHandle<()>,
    Box<dyn tracing::Subscriber + Send + Sync>,
) {
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<report_errors::ErrorLog>();

    let subscriber = tracing_subscriber::registry()
        .with(
            Logger::new(tx).with_filter(tracing_subscriber::filter::filter_fn(|m| {
                m.level() == &tracing::Level::WARN || m.level() == &tracing::Level::ERROR
            })),
        )
        .with(
            tracing_subscriber::fmt::layer()
                .with_span_events(
                    tracing_subscriber::fmt::format::FmtSpan::NEW
                        | tracing_subscriber::fmt::format::FmtSpan::CLOSE,
                )
                .with_file(true)
                .with_line_number(true)
                .with_filter(EnvFilter::from_env("LORRY_LOG")),
        );

    (
        tokio::task::spawn(async move {
            while let Some(e) = rx.recv().await {
                db.record_error(&e.error_message, &e.endpoint, e.for_lorry, e.timestamp)
                    .await
                    .ok();
            }

            eprintln!("Lorry-controller DB writer task died!")
        }),
        Box::new(subscriber),
    )
}

struct Logger {
    tx: tokio::sync::mpsc::UnboundedSender<report_errors::ErrorLog>,
}

impl Logger {
    pub fn new(tx: tokio::sync::mpsc::UnboundedSender<report_errors::ErrorLog>) -> Self {
        Self { tx }
    }
}

struct ConcatVisitor<'a> {
    concatted: BTreeMap<&'a str, String>,
}

impl ConcatVisitor<'_> {
    pub fn new() -> Self {
        ConcatVisitor {
            concatted: BTreeMap::new(),
        }
    }
}

impl tracing::field::Visit for ConcatVisitor<'_> {
    fn record_debug(&mut self, field: &tracing::field::Field, value: &dyn std::fmt::Debug) {
        //if two fields have the same name, this can overwrite the first.
        //however, I don't think tracing events allow that to occur in the first place, so this should be OK
        self.concatted.insert(field.name(), format!("{:?}", value));
    }
}

impl<S> tracing_subscriber::Layer<S> for Logger
where
    S: tracing::Subscriber,
{
    fn on_event(
        &self,
        event: &tracing::Event<'_>,
        _ctx: tracing_subscriber::layer::Context<'_, S>,
    ) {
        let mut concat_visitor = ConcatVisitor::new();
        event.record(&mut concat_visitor);

        //TODO what about other fields? We aren't recording them, if the caller happens to give them...
        //TODO also use CTX to get the span this occurred in
        //TODO turns out we can't actually do that since tracing doesn't store that into; the subscriber has to implement keeping track of that!
        let lorry_we_errored_on = concat_visitor.concatted.remove("lorry");

        let endpoint_we_errored_on = concat_visitor.concatted.remove("endpoint");

        let message = concat_visitor
            .concatted
            .remove("message")
            .expect("tracing log can't not have a message");

        let message = report_errors::ErrorLog {
            error_message: message,
            endpoint: endpoint_we_errored_on.unwrap_or("".to_string()),
            timestamp: now_in_secs(),
            for_lorry: lorry_we_errored_on.map(comms::LorryPath::new),
        };
        self.tx.send(message).expect("Logger receiver shouldn't be dropped; it's in a loop it can only exit if this transmitter is dropped!")
    }
}

/// Returns the current time in seconds
///
/// Will panic if you've got access to the TARDIS.
fn now_in_secs() -> TimeStamp {
    TimeStamp::new(
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .expect("This should not be run on pre-1970 times")
            .as_secs()
            .try_into()
            .expect("timestamp too big, consider upping the size of internal timestamps!"),
    )
}
