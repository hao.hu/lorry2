use std::fmt::Display;

use serde::{Deserialize, Serialize};
use url::Url;

/// Represents a request made by the minion of the controller. Is a `serde` type so that it can be encoded and sent over a network,
/// and requires the same of the response type. This gives guarantees as to the layout and type of the messages sent between the
/// two components.
pub trait Communication: serde::de::DeserializeOwned + serde::Serialize + std::fmt::Debug {
    /// The structure of the message that should be sent by the controller as a response to the request.
    type Response: serde::de::DeserializeOwned + serde::Serialize;
    /// The name of the endpoint on the controller that accepts this Communication as its input.
    const PATH: &'static str;
}

#[derive(Serialize, Deserialize, Debug)]
/// Sent by the worker to the controller to inform on the status of its current job
pub struct StatusUpdate {
    /// Which job is the minion carrying out?
    pub job_id: JobId,
    /// Are we informing the controller that the job was finished, and if so, what the exit code was.
    pub exit_status: JobExitStatus,
    /// The logs that have been emitted by the mirroring process since this minion last checked in.
    pub stdout: String,
}

impl Communication for StatusUpdate {
    type Response = JobInfo;

    /// Path for the endpoint for the minion to update the controller as to the status of the job it is running.
    const PATH: &'static str = "/1.0/job-update";
}

/// The status of a given job; either it is running (or has never been completed before), or it is completed,
/// with some exit status, and disk usage value.
#[derive(Serialize, Deserialize, PartialEq, Copy, Clone, Debug)]
pub enum JobExitStatus {
    Finished(JobFinishedResults), //TODO perhaps this could further be broken into a Success and Failure tuple
    Running,
}

#[derive(Copy, Clone, Serialize, Deserialize, Debug, PartialEq)]
pub struct JobFinishedResults {
    /// The exit code the job finished with
    pub exit_code: i64,
    /// The space on disk the internal repos take up
    pub disk_usage: DiskUsage,
}

impl JobExitStatus {
    pub fn is_success(&self) -> bool {
        matches!(
            self,
            JobExitStatus::Finished(JobFinishedResults {
                exit_code: 0,
                disk_usage: _
            })
        )
    }

    pub fn is_finished(&self) -> bool {
        match self {
            JobExitStatus::Finished(_) => true,
            JobExitStatus::Running => false,
        }
    }

    /// Construct a struct representing a successful mirror job
    pub fn success(usage: DiskUsage) -> Self {
        JobExitStatus::Finished(JobFinishedResults {
            exit_code: 0,
            disk_usage: usage,
        })
    }

    /// Construct a struct representing a generic failed mirror
    pub fn failure(usage: DiskUsage) -> Self {
        JobExitStatus::Finished(JobFinishedResults {
            exit_code: 1,
            disk_usage: usage,
        }) //TODO perhaps exit code should be related to the reason we're passing this object up?
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct JobInfo {
    pub kill: bool,
}

#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, sqlx::Type)]
#[sqlx(transparent)]
pub struct DiskUsage(i64);
impl DiskUsage {
    pub fn new(usage: i64) -> Self {
        Self(usage)
    }
}

/// Sent by the worker to the controller to request a new job, if one exists
#[derive(Serialize, Deserialize, Debug)]
pub struct JobRequest {
    pub host: String, //TODO not sure if this should be OsString or what...
    pub pid: u32,
}

//TODO this does not account for an error response. What does the original do if it receives an error response... It throws an error, lovely. Could we maybe integrate the error type into the Response type?
impl Communication for JobRequest {
    type Response = Option<Job>;

    /// Path for the endpoint for the minion to request a job from the controller.
    const PATH: &'static str = "/1.0/give-me-job";
}

/// Sent by the controller to the worker to specify the job the worker should work on
#[derive(Serialize, Deserialize, Debug)]
pub struct Job {
    //The text of the job, this would be the contents of the .lorry file
    pub lorry_name: String,
    pub lorry_spec: workerlib::LorrySpec,
    pub id: JobId,
    pub path: LorryPath,
    pub purge_cutoff: TimeStamp, //If the working copies were mirrored before this date, delete them
    // It may even make sense to pre-calculate the location of the downstream git repo, rather than just pass along
    // the base URL. We have all the information to do so server-side, and would simplify the client.
    pub insecure: Option<bool>,
    pub mirror_server_base_url: Url,
}

//Represents the partial file path that identifies a lorry spec; i.e by the relative upstream path it will be mirrored to.
#[derive(
    Serialize, Deserialize, Debug, Clone, sqlx::Type, PartialEq, PartialOrd, Eq, Ord, Hash,
)]
#[sqlx(transparent)]
#[serde(transparent)]
pub struct LorryPath(String);

impl std::fmt::Display for LorryPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::ops::Deref for LorryPath {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.0.as_str()
    }
}

impl LorryPath {
    pub fn new(s: String) -> Self {
        LorryPath(s)
    }
    pub fn components(&self) -> Vec<&str> {
        self.0.split('/').collect::<Vec<_>>()
    }
}

impl<'a> From<&'a LorryPath> for gitlab::api::common::NameOrId<'a> {
    fn from(value: &'a LorryPath) -> Self {
        value.0.as_str().into()
    }
}

#[derive(Serialize, Deserialize, Copy, Clone, sqlx::Type, Debug, PartialEq)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct JobId(i64);

impl JobId {
    pub fn new(v: i64) -> Self {
        Self(v)
    }
}

impl Display for JobId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(
    Serialize,
    Deserialize,
    Copy,
    Clone,
    sqlx::Type,
    Debug,
    sqlx::FromRow,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
)]
#[sqlx(transparent)]
pub struct TimeStamp(i64);

impl std::ops::Sub for TimeStamp {
    type Output = Interval;

    fn sub(self, rhs: Self) -> Self::Output {
        Interval(self.0 - rhs.0)
    }
}

#[derive(Copy, Clone, sqlx::Type, Debug, sqlx::FromRow, PartialEq, PartialOrd)]
//TODO might even make sens to replace this with inbuilt Duration type?
#[sqlx(transparent)]

pub struct Interval(i64);
impl Interval {
    pub fn new(now: i64) -> Self {
        Self(now)
    }
}

impl<'de> Deserialize<'de> for Interval {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let d: iso8601::Duration = serde::de::Deserialize::deserialize(deserializer)?;
        match d {
            iso8601::Duration::YMDHMS {
                year,
                month,
                day,
                hour,
                minute,
                second,
                millisecond,
            } => Ok(Interval(
                millisecond as i64 / 1000
                    + second as i64
                    + minute as i64* 60
                    + hour as i64* 60 * 60
                    // We ignoring leap years and real months because that would be a pain to implement and that degree of accuracy isn't so important
                    // Who is going months between mirrors anyway?
                    + (year as i64* 365 + month as i64* 30 + day as i64) * 60 * 60 * 24,
            )),
            iso8601::Duration::Weeks(num_weeks) => {
                Ok(Interval(num_weeks as i64 * 7 * 24 * 60 * 60))
            }
        }
    }
}

impl Serialize for Interval {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        //TODO could simplify the seconds into minutes and hours but that's complex and not strictly needed
        (iso8601::Duration::YMDHMS {
            year: 0,
            month: 0,
            day: 0,
            hour: 0,
            minute: 0,
            second: self.0 as u32,
            millisecond: 0,
        })
        .serialize(serializer)
    }
}

impl std::ops::Add<Interval> for TimeStamp {
    type Output = TimeStamp;

    fn add(self, rhs: Interval) -> Self::Output {
        TimeStamp(self.0 + rhs.0)
    }
}

impl std::ops::Mul<i64> for Interval {
    type Output = Self;

    fn mul(self, rhs: i64) -> Self::Output {
        Self(self.0 * rhs)
    }
}

impl Display for Interval {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Display for TimeStamp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl TimeStamp {
    pub fn new(now: i64) -> Self {
        Self(now)
    }
}
