-- As in the previous migrations, make some fields not null so we can more confidently work with them becuase they won't need to be unwrapped.
-- Or if they do, we at least have a good idea that that is a case that actually happens and need to be handled.
CREATE TABLE temp AS
SELECT *
FROM jobs;

DROP TABLE jobs;



CREATE TABLE jobs(
    job_id INT PRIMARY KEY NOT NULL, 
    host TEXT NOT NULL, 
    pid INT NOT NULL, 
    started INT NOT NULL, 
    ended INT, 
    updated INT NOT NULL, 
    kill INT NOT NULL, 
    path TEXT NOT NULL, 
    exit INT, 
    disk_usage INT, 
    output TEXT
);



INSERT INTO jobs (job_id, host, pid, started, ended, updated, kill, path, exit, disk_usage, output)
SELECT job_id, host, pid, started, ended, updated, kill, path, exit, disk_usage, output FROM temp;

DROP TABLE temp;
