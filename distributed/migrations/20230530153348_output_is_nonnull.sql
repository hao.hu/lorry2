-- As in the previous migrations; we'd like to make sure these fields cannot be null ever so we don't have to check that an unwrap is safe.
CREATE TABLE temp AS
SELECT *
FROM jobs;

DROP TABLE jobs;



CREATE TABLE jobs(
    job_id INT PRIMARY KEY NOT NULL, 
    host TEXT NOT NULL, 
    pid INT NOT NULL, 
    started INT NOT NULL, 
    ended INT, 
    updated INT NOT NULL, 
    kill INT NOT NULL, 
    path TEXT NOT NULL, 
    exit INT, 
    disk_usage INT, 
    output TEXT NOT NULL
);



INSERT INTO jobs (job_id, host, pid, started, ended, updated, kill, path, exit, disk_usage, output)
SELECT job_id, host, pid, started, ended, updated, kill, path, exit, disk_usage, output FROM temp;

DROP TABLE temp;
