-- Add a field to lorries giving a date giving the last time we sent the con troller a signal saying that we believe that this lorry's internals are corrupted
-- Sometimes, git will not recover from corruption (i.e the git tree files have been corrupted), the only way to recover is to manually delete the local files from disk
-- When we mirror this lorry, we check that the internal copy does predate the date in the `purge_from_before` column. 
-- If the repo was last updated before this date, then it could well be corrupted, and will be deleted
CREATE TABLE temp AS
SELECT *
FROM lorries;

DROP TABLE lorries;

CREATE TABLE lorries(
    path TEXT PRIMARY KEY NOT NULL, 
    text TEXT NOT NULL, 
    from_trovehost TEXT NOT NULL, 
    from_path TEXT NOT NULL, 
    running_job INT, 
    last_run INT NOT NULL, 
    interval INT NOT NULL, 
    lorry_timeout INT NOT NULL, 
    disk_usage INT, 
    last_run_exit INT, 
    last_run_error TEXT,
    purge_from_before INT NOT NULL
);

INSERT INTO lorries (path, text, from_trovehost, from_path, running_job, last_run, interval, lorry_timeout, disk_usage, last_run_exit, last_run_error, purge_from_before)
SELECT path, text, from_trovehost, from_path, running_job, last_run, interval, lorry_timeout, disk_usage, last_run_exit, last_run_error, 0 FROM temp;

DROP TABLE temp;