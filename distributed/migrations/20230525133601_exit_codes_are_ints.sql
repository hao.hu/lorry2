-- Exit codes are integers, and we should assert this on the DB level rather than pinky swearing to always put integer-parsable strings into the DB
ALTER TABLE jobs 
       ADD COLUMN exit_temp INT;

UPDATE jobs 
       SET exit_temp = CAST(exit as INT);

ALTER TABLE jobs DROP COLUMN exit;

ALTER TABLE jobs RENAME COLUMN exit_temp TO exit;