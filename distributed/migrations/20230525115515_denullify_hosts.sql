-- Tries to set a bunch of columns as NOT NULL to make sqlx Rust code clearer as to what the types of data are
-- rather than having to unwrap fields becuase you "know" they're not null becuase you searched for all the points it can be set
CREATE TABLE temp AS
SELECT *
FROM hosts;

DROP TABLE hosts;

CREATE TABLE hosts(
    host TEXT PRIMARY KEY NOT NULL, 
    protocol TEXT NOT NULL, 
    username TEXT NOT NULL, 
    password TEXT, type TEXT NOT NULL, 
    type_params TEXT NOT NULL, 
    lorry_interval INT NOT NULL, 
    lorry_timeout INT NOT NULL, 
    ls_interval INT NOT NULL, 
    ls_last_run INT, 
    prefixmap TEXT NOT NULL, ignore TEXT 
);

INSERT INTO hosts (host,protocol,username,password,type_params,lorry_interval,lorry_timeout,ls_interval,ls_last_run,prefixmap)
SELECT host,protocol,username,password,type_params,lorry_interval,lorry_timeout,ls_interval,ls_last_run,prefixmap FROM temp;

DROP TABLE temp;