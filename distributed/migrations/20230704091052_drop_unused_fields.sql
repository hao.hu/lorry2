-- Remove tables and columns related to old legacy features, in particular, those related to the Trove system

ALTER TABLE lorries DROP from_trovehost;
ALTER TABLE lorries DROP from_path;


DROP TABLE hosts;